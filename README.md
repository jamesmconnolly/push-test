/*
Project Name: Push Test
Author: James Connolly
*/

--------

FICTICIOUS PROBLEM:
A client would like to add a section to a page on their website. This page section will be full bleed (100% width) and take up at least 70% of the browser's height. It will feature a slider/counsel of social Instagram posts from a hashtag of #dogs. Each image will need to fill out the full space of the section when it is shown. The images may be cropped to fit inside of the section. The backgrounds will need to have a dark overlay on them so that the text of each Instagram post can be displayed on top in white. The content should display centered perfectly inside each slide. The entire section should also be responsive, or look good on smaller screens. 

---------

Before diving into developing a social media feed that meets the desired characteristics described above, I first created a new project folder with some basic files needed for me to get started; including a style.scss file so that my stylesheet will sync accordingly as I add normalization and typography classes to my new project.

Next, understanding that I would not be able to begin the project effectively without a working instagram account to work within, created a simple seven image instagram account for me to plug into the social media feed of my choosing, in addition to ensuring the #dogs filter would work effectively.

With both preliminary facets of the project complete, I begin to investigate juicer.io; beyond the fact it was the singular resource recommended to me at the beginning of the project, but would serve as a stepping stone for further research into javascript libraries and resources similar to juicer. However, once I looked into some of the capabilities and live examples of juicer on the web provided by the library's homepage, I realized this was the best and easiest option to fulfill the project's demands, including: full width slider showcasing one instagram post at a time, filtered by the #dogs hashtag.

I created a free account, plugged in the information to my sample instagram, and played with the juicer UI. within minuets, I was able to construct a full-width, one-post slider with toggled colors to meet all but one specification of the project when hovering over the active slide. Once I copied the embed code to add it other project folder, I ascertained which items were still needed to complete all of the project's requirements: set the slider's height to be at least 70% of the active viewport, darken the hoverstate background, and condense the vertically centered content to be more readable.

Once I embedded the code from juicer, I was able to remove the plugin's warermark, darken the background, and reduce the width of the vertically centered info text on each post. This was achieved rather easily with additional SCSS. I learned exactly what classes I needed to adjust through the Chrome browser's inspect element.

Similarly to the solutions above, inspect element proved to be a crucial part of adjusting the slider's height, however this proved to be my biggest challenge. The height of the slider can be set to a pixel value through the juicer UI, however it will default into a 300px height when no value is specified. I learned this value by inspecting the elements of the site, and learned the two tags/classes responsible for automatically setting the height through inline styles. Wanting to eschew away from additional javascript to override the inline-set height, I tinkered with a number of specificity models in the SCSS until I found one that successfully overridden the inline styles and set the class .slide-track and the <img> tags height from 300px to 70vh. This modern CSS technique ensures the height of the slider will always be 70% of the viewport height; a potentially problematic dimension for some viewports, but one that looked and functioned beautifully when observing it through various heights and widths through inspect element.

Once completed, I set up a repository on my personal Bitbucket account and push my work up for anyone to see.